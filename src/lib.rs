use chrono::{DateTime, FixedOffset, Local, Utc};
use log::{debug, info, warn};
use std::process::Command;

pub trait TimeManager {
    fn set_system_time(iso_time: &str) -> bool;

    fn get_timezone_offset() -> FixedOffset {
        let local_time = Local::now();
        let fixed_offset = local_time.offset();
        debug!(
            "Time zone offset: {} hours",
            fixed_offset.local_minus_utc() / 60 / 60
        );

        *fixed_offset
    }

    fn parse_iso_time(iso_time: &str) -> DateTime<Local> {
        let date_time = DateTime::parse_from_rfc3339(iso_time).unwrap();
        let utc_time = date_time.with_timezone(&Utc);
        let local_time = DateTime::from_naive_utc_and_offset(utc_time.naive_utc(), Self::get_timezone_offset());
        debug!("Parsed '{}' to local time: {}", iso_time, local_time);

        local_time
    }

    fn to_local_time(date_time: DateTime<FixedOffset>) -> DateTime<Local> {
        let utc_time = date_time.with_timezone(&Utc);
        let local_time = DateTime::from_naive_utc_and_offset(utc_time.naive_utc(), Self::get_timezone_offset());
        debug!("Converted {} to local time: {}", date_time, local_time);

        local_time
    }
}

pub struct ShellTimeManager;

impl TimeManager for ShellTimeManager {
    fn set_system_time(iso_time: &str) -> bool {
        let old_local_time = Local::now();
        info!("Current system time: {}", old_local_time);

        let new_local_time = Self::parse_iso_time(iso_time);
        let time_str = new_local_time.format("%Y-%m-%d %H:%M:%S").to_string();

        let output = Command::new("date")
            .arg("-s")
            .arg(time_str)
            .output()
            .expect("Failed to execute date command.");

        let exit_code = output.status.code();
        match exit_code {
            Some(0) => {
                let status = Command::new("hwclock")
                    .arg("--systohc")
                    .status()
                    .expect("Failed to execute hwclock command.");
                info!(
                    "Synchronized with internet time: {}, {}",
                    new_local_time, status
                );
            }
            Some(1) => warn!("Privilege escalation is required for this operation."),
            Some(i) => warn!("Unknown exit code: {}", i),
            _ => (),
        }

        output.status.success()
    }
}
