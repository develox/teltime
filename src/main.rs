use clap::{Arg, Command, ArgAction};
use log::{debug, error};
use regex::Regex;
use std::time::Duration;
use std::{env, process, str};
use telnet::{Event, Telnet};

use teltime::{ShellTimeManager, TimeManager};

const NAME: &str = "Teltime";
const VERSION: &str = "0.1.1";
const ABOUT: &str = "- NTP sync over Telnet";
const AUTHOR: &str = "(c) 2020-2023 Develox Solutions";

const NTP_SERVER: &str = "time.nist.gov";

const TIMEOUT: u64 = 5000; // ms

fn main() {
    let matches = Command::new(NAME)
        .version(VERSION)
        .about(ABOUT)
        .author(AUTHOR)
        .arg(
            Arg::new("verbose")
                .short('v')
                .long("verbose")
                .help("Show logs")
                .action(ArgAction::SetTrue),
        )
        .get_matches();

    let verbose_mode = matches.get_flag("verbose");

    if verbose_mode {
        env::set_var("RUST_LOG", "trace");
        env::set_var("RUST_BACKTRACE", "1");
        println!("{}\n{}\n", NAME, ABOUT);
    } else {
        env::set_var("RUST_LOG", "off");
    }
    env_logger::init();

    let response = telnet_ntp();

    let mut ok = false;

    if let Some(time_string) = parse_date(response.as_str()) {
        ok = ShellTimeManager::set_system_time(time_string.as_str());
    }

    if ok {
        if !verbose_mode {
            println!("Time is synchronized.");
        }
    } else {
        if !verbose_mode {
            println!("Failed to synchronize time, run in verbose mode for details.");
        }
        process::exit(1);
    }
}

fn telnet_ntp() -> String {
    let mut connection =
        Telnet::connect((NTP_SERVER, 13), 256).expect("Couldn't connect to the server...");

    let event = connection
        .read_timeout(Duration::from_millis(TIMEOUT))
        .expect("Read Error");

    let response: String;

    match event {
        Event::Data(buffer) => {
            response = String::from(str::from_utf8(&buffer).unwrap());
            debug!("{} returns: {:?}", NTP_SERVER, response);
        }
        _ => panic!("Failed to get response."),
    }

    response
}

fn parse_date(response: &str) -> Option<String> {
    let re = Regex::new(r"(\d{2})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})").unwrap();
    if re.is_match(response) {
        let caps = re.captures(response).unwrap();

        let y2k = |i| 2000 + i;
        let year = (caps[1]).parse::<i32>().unwrap();

        // ISO 8601
        let time_string = format!(
            "{}-{}-{}T{}:{}:{}Z",
            y2k(year),
            &caps[2],
            &caps[3],
            &caps[4],
            &caps[5],
            &caps[6]
        );

        Some(time_string)
    } else {
        error!("Unrecognized results.");
        None
    }
}

#[test]
fn test_parse_date() {
    const TEST_STR: &str = "\n58909 20-03-01 03:46:03 58 0 0 938.5 UTC(NIST) * \n";
    let some_date = parse_date(TEST_STR);
    assert!(some_date.is_some())
}
