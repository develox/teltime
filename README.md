# teltime

Teltime synchronizes time through telnet\*, in case NTP is blocked.

\* service provided by time.nist.gov

## Build

```sh
# add MUSL target
rustup target add x86_64-unknown-linux-musl

# build with MUSL target
cargo build --release --target x86_64-unknown-linux-musl
```

## Usage

```sh
sudo ./teltime

# verbose mode
sudo ./teltime -v
```
